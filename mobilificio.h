#ifndef MOBILIFICIO_H
#define MOBILIFICIO_H

#define LUNGH_COD_FISC 17
#define MAX_LENGHT_NAME 20
#define MAX_LENGHT_STREET 50

#define MOBILI_MAX_SIZE 10
#define CLIENT_MAX_SIZE 50

#define MAX_PREVENTIVE 50

// Voci del menu
#define EXIT 0
#define INSERT_PREVENTIVO 1
#define VISUALIZZA_PREVENTIVO 2
#define VISUALIZZA_CLIENTI 3

// Codici di errore
#define OK 0
#define NO_ITEM_FOUND -1
#define FULL_ARCHIVE -2

typedef struct{
    char city[MAX_LENGHT_NAME];
    char street[MAX_LENGHT_STREET];
    int number;
    char nation[MAX_LENGHT_NAME];
}address;

typedef struct{
    char codiceFiscale[LUNGH_COD_FISC];
    char name[MAX_LENGHT_NAME];
    char surname[MAX_LENGHT_NAME];
    address address;
}cliente;

typedef struct{
    cliente cliente[CLIENT_MAX_SIZE];
    int size;
}clienti;

typedef struct{
    int day;
    int month;
    int year;
}date;

typedef struct{
    char name[MAX_LENGHT_NAME];
    char type[MAX_LENGHT_NAME];
    float price;
    int quantity;
}mobile;

typedef struct{
    date data;
    char codiceFiscale[LUNGH_COD_FISC];
    int sconto;
    mobile listaMobili[MOBILI_MAX_SIZE];
    int size;
}preventivo;

typedef struct{
    preventivo listaPreventivi[MAX_PREVENTIVE];
    int progress;
}preventivi;

// Funzioni di inizializzazione archivi
void initClient(clienti*);
void initPreventivi(preventivi*);

// Funzioni relative al cliente
int searchClientByCF(clienti, char*);
cliente addNewClient();
void addClientToArchive(cliente, clienti*);
void stampaCliente(clienti, int);
void stampaListaClienti(clienti);

// Fuznioni relative all'archivio
int addNewPreventivo(preventivi*, cliente);
mobile addNewMobile();
void stampaPreventiviPerCliente(preventivi*, clienti);
float calcolaSconto(int, float);

#endif
