#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mobilificio.h"

void initClient(clienti* _clienti) {
    _clienti->size = 0;
}

void initPreventivi(preventivi* _preventivi) {
    _preventivi->progress = 0;
}

int searchClientByCF(clienti _clienti, char* codiceFiscale) {
    int index;
    index = 0;

    while((index < _clienti.size) && (strcmp(codiceFiscale, _clienti.cliente[index].codiceFiscale) != 0))
        index++;

    if (index >= _clienti.size)
        return NO_ITEM_FOUND;

    return index;
}

cliente addNewClient() {
    cliente _newClient;
    system("clear");
    printf("Inserire codice fiscale [MAX 16 cifre]: ");
    scanf("%s",_newClient.codiceFiscale);
    printf("Inserire nome: ");
    scanf("%s",_newClient.name);
    printf("Inserire cognome: ");
    scanf("%s",_newClient.surname);
    printf("Inserire la città: ");
    scanf("%s", _newClient.address.city);
    printf("Inserire la via: ");
    getchar();
    fgets(_newClient.address.street, MAX_LENGHT_STREET, stdin);
    _newClient.address.street[strlen(_newClient.address.street) - 1] = '\0';
    printf("Inserire il numero civico: ");
    scanf("%d", &_newClient.address.number);
    printf("Inserire la nazione: ");
    scanf("%s", _newClient.address.nation);
    return _newClient;
}

void addClientToArchive(cliente _cliente, clienti *_clienti) {
    int size;
    size = _clienti->size;
    strcpy(_clienti->cliente[size].codiceFiscale, _cliente.codiceFiscale);
    strcpy(_clienti->cliente[size].name, _cliente.name);
    strcpy(_clienti->cliente[size].surname, _cliente.surname);
    strcpy(_clienti->cliente[size].address.city, _cliente.address.city);
    strcpy(_clienti->cliente[size].address.street, _cliente.address.street);
    strcpy(_clienti->cliente[size].address.nation, _cliente.address.nation);
    _clienti->cliente[size].address.number = _cliente.address.number;
    _clienti->size++;
}

int addNewPreventivo(preventivi *archPreventivi, cliente newCliente) {

    if(archPreventivi->progress < MAX_PREVENTIVE){
        int index, choice,i;
        i = 0;
        mobile _mobili[MOBILI_MAX_SIZE];
        index = archPreventivi->progress;
        preventivo *tmpPrev;
        tmpPrev = &archPreventivi->listaPreventivi[index];
        system("clear");
        printf("===============================================\n");
        printf("\t\tDATI PREVENTIVO\n\n");
        printf("Inserire la data nel formato [dd mm aaaa]: ");
        scanf("%d %d %d", &tmpPrev->data.day, &tmpPrev->data.month, &tmpPrev->data.year);
        printf("Inserire il tasso di sconto in percentuale: ");
        scanf("%d",&tmpPrev->sconto);
        strcpy(tmpPrev->codiceFiscale, newCliente.codiceFiscale);
        printf("Quanti mobili vuoi inserire in lista?: ");
        scanf("%d",&choice);
        while(i < choice) {
            _mobili[i] = addNewMobile();
            i++;
        }
        //tmpPrev.listaMobili = _mobili;
        tmpPrev->size = choice;
        memcpy(tmpPrev->listaMobili, _mobili, sizeof(_mobili));
        archPreventivi->progress++;
        return OK;
    } else
        return FULL_ARCHIVE;

}

mobile addNewMobile() {
    mobile _newMobile;
    system("clear");
    printf("Inserire la tipologia (lusso, rustico, economico, ecc): ");
    scanf("%s", _newMobile.type);
    printf("Inserire il nome del mobile: ");
    scanf("%s", _newMobile.name);
    printf("Inserire il prezzo: ");
    scanf("%f", &_newMobile.price);
    printf("Inserire la quantità: ");
    scanf("%d", &_newMobile.quantity);
    return _newMobile;
}

void stampaCliente(clienti _clienti, int index) {
    cliente _client;
    _client = _clienti.cliente[index];
    system("clear");
    printf("Codice Fiscale: %s\n",_client.codiceFiscale);
    printf("Nome: %s\n",_client.name);
    printf("Cognome: %s\n",_client.surname);
    printf("Città: %s\n",_client.address.city);
    //_client.address.street[strlen(_client.address.street - 1)] = 0;
    printf("Via: %s\n",_client.address.street);
    printf("Numero civivo: %d\n", _client.address.number);
    printf("Nazione: %s\n",_client.address.nation);
    printf("\nPremere un tasto per continuare...");
    getchar();
    while(getchar() != '\n');
}

void stampaPreventiviPerCliente(preventivi *_preventivi, clienti _clienti) {
    char code[LUNGH_COD_FISC];
    int result, index, listaMobili,i;
    float sconto;
    index = 0;
    system("clear");
    printf("Inserire codice fiscale del cliente: \n");
    scanf("%s", code);
    result = searchClientByCF(_clienti, code);

    if (result < 0) {
        printf("Il cliente non è presente in archivio.\n");
        return;
    }
    puts("==============================================");
    printf("\t\t\t PREVENTIVI %s %s \n", _clienti.cliente[result].name, _clienti.cliente[result].surname);
    puts("==============================================");
    printf("\n");
    puts("Mobile\t\t\tCosto Effettivo\t\tCosto Scontato");
    puts("----------------------------------------------");
    while(index < _preventivi->progress) {
        if(strcmp(_preventivi->listaPreventivi[index].codiceFiscale, code) == 0) {
          listaMobili = _preventivi->listaPreventivi[index].size;
            for(i = 0; i < listaMobili; i++) {
                sconto = calcolaSconto(_preventivi->listaPreventivi[index].sconto, _preventivi->listaPreventivi[index].listaMobili[i].price);
                printf("%-25s %.2f %10.2f\n", _preventivi->listaPreventivi[index].listaMobili[i].name, _preventivi->listaPreventivi[index].listaMobili[i].price, sconto);
            }
        }
        index++;
    }

}

float calcolaSconto(int sconto, float prezzo) {
    float result;
    result = prezzo - (prezzo * ((float)sconto / 100));
    return result;
}

void stampaListaClienti(clienti _clienti) {
    int i, index;
    i = 0;
    index = _clienti.size;
    puts("===================================================");
    puts("\t\t\t LISTA CLIENTI");
    puts("===================================================");
    printf("\n");
    puts("Codice Fiscale\t\tNome\t\tCognome");
    puts("---------------------------------------------------");
    while(i < index) {
        printf("%-20s %s %5s\n",_clienti.cliente[i].codiceFiscale, _clienti.cliente[i].name, _clienti.cliente[i].surname);
        i++;
    }
}