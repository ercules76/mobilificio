#include <stdio.h>
#include <stdlib.h>
#include "mobilificio.h"

int main(int argc, char* argv[]) {

    int choice, result;
    char response;
    char codiceFiscale[LUNGH_COD_FISC];
    preventivi archPreventivi;
    clienti archClienti;
    cliente newCliente;

    initPreventivi(&archPreventivi);
    initClient(&archClienti);

    do {
        system("clear");
        printf("========== MOBILIFICIO ==========\n");
        printf("\n1. Inserimento nuovo preventivo.\n");
        printf("2. Visualizza preventivo per cliente.\n");
        printf("3. Visualizza tutti i clienti.\n");
        printf("0. Esci dal programma.\n\n");
        printf("=================================\n\n");
        printf("\n\nScelta: ");
        scanf("%d",&choice);

        switch(choice) {
            case INSERT_PREVENTIVO:
                printf("\nInserisci codice fiscale cliente ");
                scanf("%s",codiceFiscale);
                result = searchClientByCF(archClienti, codiceFiscale);
                if (result < 0) {
                    printf("Il cliente non è presente in archivio.\n");
                    printf("\nSi vuole continuare con l'operazione (s = Si - n = No)?\n");
                    getchar();
                    scanf("%c", &response);
                    if ((response == 's') || (response == 'S')) {
                        newCliente = addNewClient();
                        addClientToArchive(newCliente, &archClienti);
                        result = addNewPreventivo(&archPreventivi, newCliente);
                        switch(result) {
                            case OK:
                                printf("\nPreventivo salvato in archivio.\n");
                                break; // OK
                            case FULL_ARCHIVE:
                                printf("Non è possibile inserire il preventivo, archivio pieno.\n");
                                break; //FULL_ARCHIVE
                            default:
                                break;
                        }
                    }
                    printf("\nPremere un tasto per continuare...");
                    getchar();
                    while(getchar() != '\n');
                } else {
                    stampaCliente(archClienti, result);
                    printf("I dati sono corretti (s = Si - n = No)? ");
                    scanf("%c", &response);
                    if((response == 's') || (response == 'S')) {
                        newCliente = archClienti.cliente[result];
                        addNewPreventivo(&archPreventivi, newCliente);
                    }
                }
                break; // INSERT_PREVENTIVO
            case VISUALIZZA_PREVENTIVO:
                stampaPreventiviPerCliente(&archPreventivi, archClienti);
                printf("\nPremere un tasto per continuare...");
                getchar();
                while(getchar() != '\n');
                break; // VISUALIZZA_PREVENTIVO
            case VISUALIZZA_CLIENTI:
                system("clear");
                stampaListaClienti(archClienti);
                break;
            case EXIT:
                printf("Grazie per aver usato il nostro software.\n\n");
                printf("\nPremere un tasto per continuare...");
                getchar();
                while(getchar() != '\n');
                break;
            default:
                break;
        }
    }while(choice != 0);

    return 0;
}
